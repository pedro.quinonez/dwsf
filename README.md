# DOCKER WEB SERVER FARM | Platform for development and testing projects with 

# |TAMAÑOS|
# The largest heading
## The second largest heading
###### The smallest heading

# |TIPOS TEXTO|
**This is bold text**

*This text is italicized*

~~This was mistaken text~~

**This text is _extremely_ important**

# |QUOTA TEXTO|
In the words of Abraham Lincoln:

> Pardon my French

# |QUOTA CODIGO|
Use `git status` to list all new or modified files that haven't yet been committed.

Some basic Git commands are:
```
git status
git add
git commit
```
# |LINKS|
This site was built using [GitHub Pages](https://pages.github.com/).

# |LISTS|
- George Washington
- John Adams
- Thomas Jefferson

1. James Madison
2. James Monroe
3. John Quincy Adams

1. First list item in first line.
   - First nested list item
     - Second nested list item

- [x] Finish my changes
- [ ] Push my commits to GitHub
- [ ] Open a pull request


@github/support What do you think about these updates?